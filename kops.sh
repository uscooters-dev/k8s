#!/bin/sh

if [ ! -f .env ]; then
  echo "Please define and fill .env file in current directory from .env.tmpl file"
  exit 1
fi

. ./.env

kops create cluster \
  --api-loadbalancer-type internal \
  --bastion="true" \
  --cloud aws \
  --cloud-labels "Environment=\"production\",Type=\"k8s\",Role=\"node\",Provisioner=\"kops\"" \
  --dns public \
  --dns-zone $dns \
  --encrypt-etcd-storage \
  --kubernetes-version 1.9.8 \
  --master-size t2.small \
  --master-zones $zones \
  --networking calico \
  --name $NAME \
  --node-count 1 \
  --node-size t2.medium \
  --ssh-public-key ./ssh/id_rsa.pub \
  --topology private \
  --vpc $vpc \
  --zones $zones \
  "$@"
