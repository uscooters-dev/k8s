#!/bin/sh

# add next line to /etc/hosts
# 127.0.0.1 api.$NAME

set -ex

if [ -z "$KOPS_STATE_STORE" ]; then
  echo "Cluster configuration not imported, please do"
  echo
  echo ". ./env"
  exit 1
fi

SSH_PRIVATE=ssh/id_rsa

ssh -o ProxyCommand="ssh admin@bastion.$NAME -i $SSH_PRIVATE exec nc %h %p" -i $SSH_PRIVATE admin@api.internal.$NAME
