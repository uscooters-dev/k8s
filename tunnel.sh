#!/bin/sh

set -xe

sudo ssh -L 127.0.0.1:443:api.usctr.co:443 admin@bastion.usctr.co -i ssh/id_rsa  "$@"
